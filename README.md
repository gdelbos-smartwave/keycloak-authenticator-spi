# Authentication Provider for BGER

## Presentation

When a user opens a session on is desktop, most application are available.
However, application protected by Keycloak remains unauthorized until the user login manually using its username and password.

The aim of this project is to allow an automatic login for the user.

### Requirements

 - Maven 3
 - Java 11
 - Keycloak 15.0.2

### Use case presentation

```mermaid
sequenceDiagram
	autonumber
	participant Desktop
	participant FF as Firefox
	participant kcbger as "Keycloak bger-auth"
	participant kcaccount as "Keycloak account"
	participant kcidp as IDP
	
Desktop ->> Desktop: user opens session
Desktop ->> Desktop: session caches credentials
Desktop ->> Desktop: login script runs curl with credentials
Desktop ->> kcidp: curl authN: protocol/openid-connect/token + client_id=admin-cli

activate kcidp
alt IdP: Is authN OK ?
	kcidp -->> Desktop: 401 KO
else
	kcidp ->> Desktop: 200 OK + JWT 60sec
end
deactivate kcidp

Desktop ->> Desktop: store JWT in temp variable
Desktop ->> FF: login script opens firefox with bger-auth URL + JWT

Desktop ->> Desktop: unset temp variable

Desktop -->> Desktop: start loop Check KC cookie date
activate Desktop
	
	FF ->> kcbger: open page bger-auth URL
	kcbger ->> FF: temporary cookie JWT+RemberMe=true
	FF ->> FF: store cookie AAABGERAUTHCOOKIE
	FF ->> kcaccount: redirect
		
	activate kcaccount
	alt Account Is connected?
		kcaccount -->> FF: OK Response 200
		FF -->> FF: 200 end AuthN
	else
		kcaccount ->> FF: KO Response 302
	end
	deactivate kcaccount
	
	FF ->> kcidp: redirect with AAABGERAUTHCOOKIE
	kcidp ->> kcidp: authentication with AAABGERAUTHCOOKIE cookie
	
	activate kcidp
	alt IdP: Is authN OK ?
		kcidp -->> kcidp: KO : login page user + pwd
	else
		kcidp ->> FF: KC cookie + AAABGERAUTHCOOKIE expired
	end
	deactivate kcidp
	
	FF ->> FF: store KC cookie
	FF ->> kcaccount: redirect
	
	Desktop -->> Desktop: end loop Check KC cookie date after 60sec
deactivate Desktop

Desktop ->> Desktop: close FF

```

### Additional information

#### Retrieve JWT

The 4th step concerns the retrieval of a JWT. This JWT can be retrieve with a simple cURL command:

```bash
curl --location --request POST 'https://kc.bger.admin.ch/auth/realms/generic-user/protocol/openid-connect/token' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data-urlencode 'username=THE_USERNAME' \
    --data-urlencode 'password=THE_PASSWORD' \
    --data-urlencode 'client_id=admin-cli' \
    --data-urlencode 'grant_type=password' \
    --data-urlencode 'scope=openid'
```

Just replace:

 - the value `THE_USERNAME` by the correct username;
 - the value `THE_PASSWORD` by the correct password.

### Page to open in Firefox

The URL to open in firefox is the following:
`https://kc.bger.admin.ch/auth/realms/generic-user/bger-auth?rememberme=true&token=THE_JWT_TOKEN`

Just replace:

 - the value `THE_JWT_TOKEN` by the JWT retrieve from the cURL call.

## Auto login extension

This project will manage:
 - the `bger-auth` resource in Keycloak
 - the 19th step (`authentication with AAABGERAUTHCOOKIE cookie`).

### Installation

The installation of the plugin is quite simple. If you already have the `tar.gz` archive place it in the `/modules` folder of your keycloak installation.
Then follow the following steps starting at the 3rd point.

 - Package the project using maven: `mvn clean package`;
 - Retrieve the archive named `keycloak-authenticator-spi-1.0.0-assemblyModule.tar.gz` present in the folder `target` and place it in the `/modules` folder of your keycloak installation;
 - Open the Keycloak configuration file (depends on your installation: `standalone.xml`, `standalone-ha.xml` or `domain.xml`)
 - Search for the subsystem declaration of `urn:jboss:domain:keycloak-server:1.1`
   - It should start with `<web-context>auth</web-context>`
   - It should contain an element `providers`
 - Complete the `providers` by adding this: `<provider>module:ch.bger.keycloak.authenticator.spi</provider>`
   - The `providers` element should resemble something like this:
    ```xml
    <providers>
        <provider>classpath:${jboss.home.dir}/providers/*</provider>
        <provider>module:ch.bger.keycloak.authenticator.spi</provider>
    </providers>
    ```
 - Restart Keycloak server

### Configuration

The plugin has two types of configuration. 
One is an offline and concerns the `bger-auth` resource. 
The second one is a runtime configuration and concerns the authentication management.

#### Offline configuration

The `bger-auth` resource comes with three configurable elements:

 - `cookieName` the name of the cookie that will be used to share the JWT with the authentication process. The default value is `AAABGERAUTHCOOKIE`;
 - `cookieSecured` if the cookie must be presented by the browser only in case of secured connection. The default value is `true`;
 - `redirectPath` the URL of the keycloak protected resource that should be open after the user has finalized the authentication. The default value is `https://kc.bger.admin.ch/auth/realms/generic-user/account/#/personal-info`

Concerning the `redirectPath`, a good value should respond positively to the following constraint:

 - You are automatically redirected to the Keycloak authentication form when calling this resource in a Private Mode navigation;
 - You are correctly seeing the page when you have already authenticated yourself against Keycloak.

WARNING: When changing this configuration, you have to restart the Keycloak server.

 - Open the Keycloak configuration file (depends on your installation: `standalone.xml`, `standalone-ha.xml` or `domain.xml`)
 - Search for the subsystem declaration of `urn:jboss:domain:keycloak-server:1.1`
 - Ensure that there is no `realm-restapi-extension` SPI declaration:
    - If at least on declaration, add the following lines to the existing element:
      ```xml
      <provider name="bger-auth" enabled="true">
          <properties>
              <property name="cookieName" value="AAABGERAUTHCOOKIE"/>
              <property name="cookieSecured" value="true"/>
              <property name="redirectPath" value="https://kc.bger.admin.ch/auth/realms/generic-user/account/#/personal-info"/>
          </properties>
      </provider>
      ```
    - If no SPI declaration, at the end of the element `subsytem` (after all the other `spi` declaration) add the following line:
      ```xml
      <spi name="realm-restapi-extension">
          <provider name="bger-auth" enabled="true">
              <properties>
                  <property name="cookieName" value="AAABGERAUTHCOOKIE"/>
                  <property name="cookieSecured" value="true"/>
                  <property name="redirectPath" value="https://kc.bger.admin.ch/auth/realms/generic-user/account/#/personal-info"/>
              </properties>
          </provider>
      </spi>
      ```
 - Replace the configuration you wish to change. You may also remove any unchanged configuration.


#### Runtime configuration

This configuration requires an admin access to the Keycloak server configuration.

 - Open the administration console (`https://kc.bger.admin.ch/auth/admin/master/console` for instance);
 - Select the `generic-user` realm;
 - Open the `Authentication` configuration
 - Open the `Bindings` panel and check the selected value for `Browser Flow`;
 - Open the `Flows` panel and switch to the correct flow;
 - Create a copy of this flow by clicking the `Copy` button (chose the name of the new copy);
 - Click the `Add execution` button;
 - Select `BGER Token Authentication` in the menu and click the `Save` button;
 - Ensure the newly added execution is the second execution (just after the `Cookie` execution);
 - Open the `Bindings` and modify `Browser Flow` to point on the flow you have just updated;
 - Click the `Save` button.

The Keycloak configuration is now in done.

## Troubleshooting

### The `BGER Token Authentication` option is not present!

This means that the module was not correctly deployed. 

 - Check the installation steps
 - Ensure the log level is at least INFO
 - Restart the server.

In the logs you should see the lines resembling those:
```text
13:32:43,030 INFO  [ch.bger.keycloak.authenticator.services.BgerAuthResourceProviderFactory] (ServerService Thread Pool -- 65) Factory initialized with cookieName:[MY_COOKIE_NAME] cookieName:[false] redirectPath:[account]
13:32:43,374 INFO  [ch.bger.keycloak.authenticator.services.BgerTokenAuthenticatorProviderFactory] (ServerService Thread Pool -- 65) Factory initialization
```

If not, contact your support.