package ch.bger.keycloak.authenticator.services;

import org.jboss.logging.Logger;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakUriInfo;
import org.keycloak.services.resource.RealmResourceProvider;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.net.URI;

import static org.keycloak.services.util.CookieHelper.addCookie;

public class BgerAuthResourceProvider implements RealmResourceProvider {

    private static final Logger LOG = Logger.getLogger(BgerAuthResourceProvider.class);

    private static final String COOKIE_VALUE_FORMAT = "%s|%s";

    private final KeycloakSession session;
    private final String cookieName;
    private final boolean cookieSecured;
    private final String redirectPath;

    public BgerAuthResourceProvider(KeycloakSession session, String cookieName, boolean cookieSecured, String redirectPath) {
        this.session = session;
        this.cookieName = cookieName;
        this.cookieSecured = cookieSecured;
        this.redirectPath = redirectPath;
    }

    @Override
    public Object getResource() {
        return this;
    }

    @Override
    public void close() {
    }

    @GET
    @Produces("text/plain; charset=utf-8")
    public Response get(@QueryParam("token") String token, @QueryParam("rememberme") String rememberMe) {
        LOG.info("Start preparing BGER auth");
        LOG.debugf("BGER Auth parameters: token[%s] rememberme[%s]", token, rememberMe);

        KeycloakUriInfo uriInfo = session.getContext().getUri();
        URI uri = URI.create(redirectPath);
        LOG.debugf("URI info - baseUri: [%s] redirectUri: [%s]", uriInfo.getBaseUri(), uri);

        if (token != null && "true".equals(rememberMe)) {
            String value = String.format(COOKIE_VALUE_FORMAT, token, rememberMe);
            addCookie(cookieName, value, uriInfo.getBaseUri().getRawPath(), null, null, 3600, cookieSecured, true);
        }

        LOG.debugf("Launch redirect to [%s]", uri);
        return Response.status(Response.Status.FOUND).location(uri).build();
    }

    public String getCookieName() {
        return cookieName;
    }

    public boolean isCookieSecured() {
        return cookieSecured;
    }
}
