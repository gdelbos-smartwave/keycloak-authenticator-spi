package ch.bger.keycloak.authenticator.services;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.Authenticator;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.provider.ProviderFactory;
import org.keycloak.services.managers.AppAuthManager;
import org.keycloak.services.managers.AuthenticationManager;
import org.keycloak.services.resource.RealmResourceProvider;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Cookie;

import static org.keycloak.services.util.CookieHelper.addCookie;

public class BgerTokenAuthenticator implements Authenticator {

    private static final Logger LOG = Logger.getLogger(BgerTokenAuthenticator.class);

    @Override
    public void authenticate(AuthenticationFlowContext context) {
        LOG.info("Try authenticating request.");
        if (validateAccessToken(context)) {
            LOG.info("Request contains valid access token. Authentication success.");
            context.success();
        } else {
            LOG.info("Request does not contain a valid access token. Continue process with next authenticator.");
            context.attempted();
        }
    }

    private boolean validateAccessToken(AuthenticationFlowContext context) {
        BgerAuthResourceProvider provider = getProvider(context.getSession());
        LOG.debugf("BgerAuthResourceProvider is: [%s]", provider);
        String cookieName = provider.getCookieName();

        LOG.debugf("Cookie name to check: [%s]", cookieName);
        Cookie cookie = context.getHttpRequest().getHttpHeaders().getCookies().get(cookieName);
        if (cookie == null) {
            LOG.debug("Cookie not present in headers");
            return false;
        } else {
            LOG.debugf("Cookie present. Ensuring expiration so that it won't be use again.");
            addCookie(cookieName, "", context.getUriInfo().getBaseUri().getRawPath(), null, null, 0, provider.isCookieSecured(), true);
        }
        LOG.debug("Start authentication attempt.");

        String[] cookieValue = cookie.getValue().split("\\|");
        String token = cookieValue[0];
        String remember = cookieValue[1];
        LOG.debugf("Token validation start: token: [%s] rememberme: [%s]", token, remember);

        AuthenticationManager.AuthResult authResult = getAuthentication(token, context.getSession());
        if (authResult != null) {
            LOG.debug("Token validation successful.");
            context.attachUserSession(authResult.getSession());
            context.getAuthenticationSession().setAuthenticatedUser(authResult.getUser());
            return true;
        }
        LOG.debug("Token validation failed.");
        return false;
    }

    public static AuthenticationManager.AuthResult getAuthentication(String accessToken, KeycloakSession keycloakSession) {
        AppAuthManager.BearerTokenAuthenticator bearerTokenAuthenticator = new AppAuthManager.BearerTokenAuthenticator(keycloakSession)
                .setTokenString(accessToken);
        return bearerTokenAuthenticator.authenticate();
    }

    @NotNull
    public static BgerAuthResourceProvider getProvider(KeycloakSession keycloakSession) {
        ProviderFactory<RealmResourceProvider> factory = keycloakSession.getKeycloakSessionFactory().getProviderFactory(RealmResourceProvider.class, BgerAuthResourceProviderFactory.PROVIDER_ID);
        LOG.debugf("Factory is: [%s]", factory);
        if (factory instanceof BgerAuthResourceProviderFactory) {
            return (BgerAuthResourceProvider) factory.create(keycloakSession);
        } else {
            LOG.warn("Factory is not of the correct instance. Retuning null.");
            throw new IllegalStateException("Factory is not of the correct instance.");
        }
    }

    @Override
    public void action(AuthenticationFlowContext context) {
    }

    @Override
    public boolean requiresUser() {
        return false;
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {
    }

    @Override
    public void close() {
    }
}
