package ch.bger.keycloak.authenticator.services;

import org.jboss.logging.Logger;
import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.services.resource.RealmResourceProvider;
import org.keycloak.services.resource.RealmResourceProviderFactory;

public class BgerAuthResourceProviderFactory implements RealmResourceProviderFactory {

    private static final Logger LOG = Logger.getLogger(BgerAuthResourceProviderFactory.class);

    public static final String PROVIDER_ID = "bger-auth";
    public static final String DEFAULT_COOKIE_NAME = "AAABGERAUTHCOOKIE";
    public static final boolean DEFAULT_COOKIE_SECURED = true;
    public static final String DEFAULT_REDIRECT_PATH = "https://kc.bger.admin.ch/auth/realms/generic-user/account/#/personal-info";

    private String cookieName;
    private boolean cookieSecured;
    private String redirectPath;

    @Override
    public RealmResourceProvider create(KeycloakSession session) {
        return new BgerAuthResourceProvider(session, cookieName, cookieSecured, redirectPath);
    }

    @Override
    public void init(Config.Scope config) {
        cookieName = config.get("cookieName", DEFAULT_COOKIE_NAME);
        cookieSecured = config.getBoolean("cookieSecured", DEFAULT_COOKIE_SECURED);
        redirectPath = config.get("redirectPath", DEFAULT_REDIRECT_PATH);
        LOG.infof("Factory initialized with cookieName:[%s] cookieName:[%s] redirectPath:[%s]", cookieName, cookieSecured, redirectPath);
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
    }

    @Override
    public void close() {
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }
}
